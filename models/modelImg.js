const mysql = require("mysql");
const db    = require( __dirname + '/../config/database.json')[ 'production' ];

class modelImg{
    constructor(){
        //maps_img attributes
        this.id     = 0;
        this.filename = '';
        this.s3name = '';
        this.ts = '';
        this.description = '';
        this.visible = '';
        this.maps_project_id = 0;
        this.maps_category_id = 0;

        //maps_img_bounds attributes
        this.north = 0;
        this.south = 0;
        this.west = 0;
        this.east = 0;
    }

    getConexion(){
        this.connection = mysql.createConnection({
            host     : db.host,
            user     : db.username,
            password : db.password,
            database : db.database
        }); 
    }

    getID(){ return this.id; }
    getFilename(){ return this.filename; }
    getS3name(){ return this.s3name; }
    getTs(){ return this.ts; }
    getDescription(){ return this.description; }
    getVisible(){ return this.visible; }
    getProjectId(){ return this.maps_project_id; }
    getCategoryId(){ return this.maps_category_id; }
    getNorth(){ return this.north; }
    getSouth(){ return this.south; }
    getWest(){ return this.west; }
    getEast(){ return this.east; }

    setID( _id ){ this.id = _id; }
    setFilename( _filename ){ this.filename = _filename; }
    setS3name( _s3name ){ this.s3name = _s3name; }
    setTs( _ts ){ this.ts = _ts; }
    setDescription( _description ){ this.description = _description; }
    setVisible( _visible ){ this.visible = _visible; }
    setProjectId( _project_id ){ this.maps_project_id = _project_id; }
    setCategoryId( _category_id ){ this.maps_category_id = _category_id; } 
    setNorth( _north ){ this.north = _north; }
    setSouth( _south ){ this.south = _south; }
    setWest( _west ){ this.west = _west; }
    setEast( _east ){ this.east = _east; }

    insertImg(){

        return new Promise( (success, fails ) => {
            
            if( this.connection == null || this.connection.state === 'disconnected' ){
                this.getConexion();
                this.connection.connect();
            }

            let query_img = 'INSERT INTO maps_img(filename, s3name, ts, description, visible, maps_project_id, maps_category_id) ' +
                            'VALUES("'+ this.filename+'", "'+this.s3name+'", now(), "'+this.description+'", '+this.visible+', '+ this.maps_project_id+', '+this.maps_category_id+')';

            this.connection.query( query_img , ( error, results, fields ) => {
                if( error ) return fails( error ); 

                success( results );                               
            });

            this.connection.end();
        });
    }
    insertImgBounds(){
        return new Promise( ( success, fails )  => {

            
            this.getConexion();
            this.connection.connect();
                        
            let query_bounds = 'INSERT INTO maps_img_bounds(north, south, east, west, maps_img_id) ' + 
                                'VALUES('+this.north+', '+this.south+', '+ this.east+', '+ this.west+', '+this.id+')';
            
            this.connection.query( query_bounds, ( err, res, field ) => {
                if( err ) return fails( err );

                success( res );
                
            });

            this.connection.end();
            
        });
    }
}

module.exports = modelImg;